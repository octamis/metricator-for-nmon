<form script="autodiscover.js,custom_layout.js" stylesheet="ui_simple.css" isVisible="true" version="1.1" theme="dark">
    <label>NMON TOP: Resources Usage per Command Invocation</label>
    <description>TOP Processes CPU and Memory utilization using Bubble charts</description>

    <fieldset autoRun="false" submitButton="false">

        <input type="time" token="timerange" searchWhenChanged="true">
            <label>Time Range:</label>
            <default>
                <earliest>-24h</earliest>
                <latest>now</latest>
            </default>
        </input>

        <input type="dropdown" token="timefilter" searchWhenChanged="true">
            <label>Time Filtering:</label>
            <choice value="No_Filter">No Filter (24/24, 7/7)</choice>
            <choice value="Day_BusinessDays_8h-19h">Day Business (08h-19h)</choice>
            <choice value="Day_WeekEnd_8h-19h">Day WE (08h-19h)</choice>
            <choice value="Day_AllDays_8h-19h">Day Week (08h-19h)</choice>
            <choice value="Night_BusinessDays_19h-8h">Night Business (19h-08h)</choice>
            <choice value="Night_WeekEnd_19h-8h">Night WE (19h-08h)</choice>
            <choice value="Night_AllDays_19h-8h">Night All Days (19h-08h)</choice>
            <default>No_Filter</default>
        </input>

        <input type="dropdown" token="osfilter" searchWhenChanged="true">
            <label>Filter OS Type:</label>
            <default>*</default>
            <prefix>OStype=</prefix>
            <choice value="*">Any OS</choice>
            <choice value="AIX">AIX</choice>
            <choice value="Linux">Linux</choice>
            <choice value="Solaris">Solaris</choice>
        </input>

        <input id="frameID" type="multiselect" token="frameID" searchWhenChanged="true">
            <label>Frame IDs:</label>
            <!-- Populating Data Model Search -->
            <search base="populate">
                <query>stats count by frameID | dedup frameID | sort 0 frameID</query>
            </search>
            <valuePrefix>frameID="</valuePrefix>
            <valueSuffix>"</valueSuffix>
            <delimiter> OR </delimiter>
            <choice value="*">Any</choice>
            <initialValue>*</initialValue>
            <fieldForLabel>frameID</fieldForLabel>
            <fieldForValue>frameID</fieldForValue>
        </input>

        <input type="text" token="host-prefilter" searchWhenChanged="true">
            <label>Optional: Filter hosts populating</label>
            <default>*</default>
        </input>

		<input id="host" type="multiselect" token="host" searchWhenChanged="true">
		    <label>Hosts Selection:</label>
		    <!-- Populating Data Model Search -->
		    <search base="populate">
		        <query>search $frameID$ host=$host-prefilter$
	| stats count by host | dedup host | sort 0 host</query>
		    </search>
		    <valuePrefix>host="</valuePrefix>
		    <valueSuffix>"</valueSuffix>
		    <delimiter> OR </delimiter>
		    <choice value="*">ALL Hosts</choice>
		    <fieldForLabel>host</fieldForLabel>
		    <fieldForValue>host</fieldForValue>
		    <change>
		        <condition>
		            <unset token="form.host_query_input"></unset>
		        </condition>
		    </change>
		</input>

		<!-- hidden form that handles the advanced definition of hosts selection -->

		<input id="host_query_input" type="dropdown" token="host_query" searchWhenChanged="true" depends="$hidden_form$">
		    <label>Hosts Selection:</label>
		    <search base="populate">
		        <query>search $host$ $frameID$
	| stats values(host) as host
	| format | fields - host | rename search as host</query>
		    </search>
		    <selectFirstChoice>true</selectFirstChoice>
		    <fieldForLabel>host</fieldForLabel>
		    <fieldForValue>host</fieldForValue>
		</input>

        <input type="dropdown" token="statsmode" searchWhenChanged="true">
            <label>Stats mode:</label>
            <default>avg</default>
            <choice value="max">Max</choice>
            <choice value="avg">Avg</choice>
            <choice value="min">Min</choice>
            <choice value="median">Median</choice>
            <choice value="mode">Mode</choice>
            <choice value="range">Range</choice>
        </input>

        <input type="text" token="dimension_Command" searchWhenChanged="true">
            <label>Optional: Filter by Command</label>
            <prefix>dimension_Command="</prefix>
            <suffix>"</suffix>
            <default>*</default>
        </input>

    </fieldset>

    <!-- Base Searches for PostProcessing -->

    <search id="populate">
        <query>| mcatalog values(metric_name) where `nmon_metrics_index` metric_name="os.unix.nmon.processes.top.*" $osfilter$ by host
| `mapping_frameID`
| stats count by frameID, host | sort 0 host</query>
        <earliest>$timerange.earliest$</earliest>
        <latest>$timerange.latest$</latest>
    </search>

    <!-- Search based for post processing -->

    <search id="tablestats_cpu_per_command">
        <query>| mstats max(_value) as value where `nmon_metrics_index` metric_name=os.unix.nmon.processes.top.pct_CPU host=$host-prefilter$ $host_query$ ($dimension_Command$) by host, dimension_Command, dimension_PID span=1m
| stats sum(value) as pct_CPU by _time, host, dimension_Command
| `$timefilter$`
| eval usage_per_core=(pct_CPU/100)
| stats $statsmode$(usage_per_core) as usage by dimension_Command
| rename dimension_Command as Command
| eval usage=round(usage, 3)
| sort - usage</query>
        <earliest>$timerange.earliest$</earliest>
        <latest>$timerange.latest$</latest>
    </search>

    <search id="tablestats_cpu_per_host">
        <query>| mstats max(_value) as value where `nmon_metrics_index` metric_name=os.unix.nmon.processes.top.pct_CPU host=$host-prefilter$ $host_query$ ($dimension_Command$) by host, dimension_Command, dimension_PID span=1m
| stats sum(value) as pct_CPU by _time, host, dimension_Command
| `$timefilter$`
| eval usage_per_core=(pct_CPU/100)
| stats $statsmode$(usage_per_core) as usage by host, dimension_Command
| rename dimension_Command as Command
| eval usage=round(usage, 3)
| sort - usage</query>
        <earliest>$timerange.earliest$</earliest>
        <latest>$timerange.latest$</latest>
    </search>

    <row>
        <panel>
            <html>
                <div class="custom">
                    <h1>CPU Single Core Usage per Command Invocation (PIDs aggregation per interval)</h1>
                </div>
            </html>
        </panel>
    </row>

    <row>
        <panel id="panela1_setWidth_50">
            <title>Graphical Representation</title>
            <html>
                <div id="bubbleSearch"
                     class="splunk-manager"
                     data-require="splunkjs/mvc/searchmanager"
                     data-options="{&quot;search&quot;:
                     {
                     &quot;type&quot;:
                     &quot;token_safe&quot;,
                     &quot;value&quot;: &quot;| mstats max(_value) as value where `nmon_metrics_index` metric_name=os.unix.nmon.processes.top.pct_CPU host=$$host-prefilter$$ $$host_query$$ ($$dimension_Command$$) by host, dimension_Command, dimension_PID span=1m
| stats sum(value) as pct_CPU by _time, host, dimension_Command
| `$$timefilter$$`
| eval usage_per_core=(pct_CPU/100)
| stats $$statsmode$$(usage_per_core) as usage by dimension_Command
| rename dimension_Command as Command
| eval usage=round(usage, 3)
| sort - usage | head 100&quot;},
                    &quot;earliest_time&quot;:
                    {&quot;type&quot;: &quot;token_safe&quot;,
                    &quot;value&quot;: &quot;$$timerange.earliest$$&quot;
                    },
                    &quot;latest_time&quot;: {&quot;type&quot;: &quot;token_safe&quot;,
                    &quot;value&quot;: &quot;$$timerange.latest$$&quot;
                    },
                    &quot;auto_cancel&quot;: 90,
                    &quot;preview&quot;: true
                    }">
                </div>
                <div id="bubbleChart" class="splunk-view" data-require="app/metricator-for-nmon/components/bubblechart/bubblechart" data-options="{
                &quot;managerid&quot;: &quot;bubbleSearch&quot;,
                &quot;nameField&quot;: &quot;Command&quot;,
                &quot;categoryField&quot;: &quot;Command&quot;,
                &quot;valueField&quot;: &quot;usage&quot;,
                &quot;height&quot;: 600
                }">
                </div>
            </html>
        </panel>
        <panel id="panela2_setWidth_25">
            <title>Stats by Commands</title>
            <table>
                <search base="tablestats_cpu_per_command">
                    <query></query>
                </search>
                <option name="wrap">true</option>
                <option name="rowNumbers">false</option>
                <option name="dataOverlayMode">none</option>
                <option name="drilldown">row</option>
                <option name="count">20</option>
            </table>
        </panel>
        <panel id="panela3_setWidth_25">
            <title>Stats by Hosts</title>
            <table>
                <search base="tablestats_cpu_per_host">
                    <query></query>
                </search>
                <option name="wrap">true</option>
                <option name="rowNumbers">false</option>
                <option name="dataOverlayMode">none</option>
                <option name="drilldown">row</option>
                <option name="count">20</option>
            </table>
        </panel>
    </row>

    <row>
        <panel>
            <html>
                <div class="custom">
                    <h1>MEMORY Statistics per Command Invocation (PIDs aggregation per interval)</h1>
                </div>
            </html>
        </panel>
    </row>

    <!-- Search based for post processing -->

    <search id="tablestats_memory_per_command">
        <query>| mstats max(_value) as value where `nmon_metrics_index` metric_name=os.unix.nmon.processes.top.ResSet OR metric_name=os.unix.nmon.processes.top.ResText OR metric_name=os.unix.nmon.processes.top.ResData host=$host-prefilter$ $host_query$ ($dimension_Command$) by metric_name, OStype, host, dimension_Command, dimension_PID span=1m
| stats sum(value) as value by _time, metric_name, OStype, host, dimension_Command
| `$timefilter$`
| `def_all_os_top_memory`
| stats sum(mem_usage_mb) As mem_usage_mb by _time, host, dimension_Command
| stats $statsmode$(mem_usage_mb) as mem_usage_mb by dimension_Command
| rename dimension_Command as Command
| eval mem_usage_mb=round(mem_usage_mb, 2)
| sort - mem_usage_mb</query>
        <earliest>$timerange.earliest$</earliest>
        <latest>$timerange.latest$</latest>
    </search>

    <search id="tablestats_memory_per_host">
        <query>| mstats max(_value) as value where `nmon_metrics_index` metric_name=os.unix.nmon.processes.top.ResSet OR metric_name=os.unix.nmon.processes.top.ResText OR metric_name=os.unix.nmon.processes.top.ResData host=$host-prefilter$ $host_query$ ($dimension_Command$) by metric_name, OStype, host, dimension_Command, dimension_PID span=1m
| stats sum(value) as value by _time, metric_name, OStype, host, dimension_Command
| `$timefilter$`
| `def_all_os_top_memory`
| stats sum(mem_usage_mb) As mem_usage_mb by _time, host, dimension_Command
| stats $statsmode$(mem_usage_mb) as mem_usage_mb by host, dimension_Command
| rename dimension_Command as Command
| eval mem_usage_mb=round(mem_usage_mb, 2)
| sort - mem_usage_mb</query>
        <earliest>$timerange.earliest$</earliest>
        <latest>$timerange.latest$</latest>
    </search>

    <row>
        <panel id="panelb1_setWidth_50">
            <title>Graphical Representation</title>
            <html>
                <div id="bubbleSearch2"
                     class="splunk-manager"
                     data-require="splunkjs/mvc/searchmanager"
                     data-options="{&quot;search&quot;:
                     {
                     &quot;type&quot;:
                     &quot;token_safe&quot;,
                     &quot;value&quot;: &quot;| mstats max(_value) as value where `nmon_metrics_index` metric_name=os.unix.nmon.processes.top.ResSet OR metric_name=os.unix.nmon.processes.top.ResText OR metric_name=os.unix.nmon.processes.top.ResData host=$$host-prefilter$$ $$host_query$$ ($$dimension_Command$$) by metric_name, OStype, host, dimension_Command, dimension_PID span=1m
| stats sum(value) as value by _time, metric_name, OStype, host, dimension_Command
| `$$timefilter$$`
| `def_all_os_top_memory`
| stats sum(mem_usage_mb) As mem_usage_mb by _time, host, dimension_Command
| stats max(mem_usage_mb) as mem_usage_mb by dimension_Command
| rename dimension_Command as Command
| eval mem_usage_mb=round(mem_usage_mb, 2)
| sort - mem_usage_mb | head 100&quot;},
                    &quot;earliest_time&quot;:
                    {&quot;type&quot;: &quot;token_safe&quot;,
                    &quot;value&quot;: &quot;$$timerange.earliest$$&quot;
                    },
                    &quot;latest_time&quot;: {&quot;type&quot;: &quot;token_safe&quot;,
                    &quot;value&quot;: &quot;$$timerange.latest$$&quot;
                    },
                    &quot;auto_cancel&quot;: 90,
                    &quot;preview&quot;: true
                    }">
                </div>
                <div id="bubbleChart2" class="splunk-view" data-require="app/metricator-for-nmon/components/bubblechart/bubblechart" data-options="{
                &quot;managerid&quot;: &quot;bubbleSearch2&quot;,
                &quot;nameField&quot;: &quot;Command&quot;,
                &quot;categoryField&quot;: &quot;Command&quot;,
                &quot;valueField&quot;: &quot;mem_usage_mb&quot;,
                &quot;height&quot;: 600
                }">
                </div>
            </html>
        </panel>
        <panel id="panelb2_setWidth_25">
            <title>Stats by Commands</title>
            <table>
                <search base="tablestats_memory_per_command">
                    <query></query>
                </search>
                <option name="wrap">true</option>
                <option name="rowNumbers">false</option>
                <option name="dataOverlayMode">none</option>
                <option name="drilldown">row</option>
                <option name="count">20</option>
            </table>
        </panel>
        <panel id="panelb3_setWidth_25">
            <title>Stats by Hosts</title>
            <table>
                <search base="tablestats_memory_per_host">
                    <query></query>
                </search>
                <option name="wrap">true</option>
                <option name="rowNumbers">false</option>
                <option name="dataOverlayMode">none</option>
                <option name="drilldown">row</option>
                <option name="count">20</option>
            </table>
        </panel>
    </row>

</form>
