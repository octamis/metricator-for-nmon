<dashboard version="1.1" theme="dark" stylesheet="manage_alerting_threshold_filesystem_v2.css,table_data_bar.css" script="manage_alerting_threshold_filesystem_v2.js,manage_alerting_threshold_filesystem_table_bar.js" hideEdit="true">
    <label>Configuration and management of per server file-system alerting threshold</label>
    <row>
        <panel>
            <html>
                <style>
                    .parent-top-single {
                        display: grid;
                        grid-template-columns: repeat(3, 1fr);
                        grid-template-rows: 1fr;
                        grid-column-gap: 0px;
                        grid-row-gap: 0px;
                    }
                </style>

                <!-- modal windows -->

                <!-- Help Modal -->
                <div class="modal custom-modal-60 fade" id="modal_help" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"></span>
                                </button>
                                <div style="text-align: left;">
                                    <h4 class="modal-title">Integrated Navigation:</h4>
                                </div>
                            </div>
                            <div class="modal-body">
                                <div style="text-align: center;">
                                    <h1>ALERTING THRESHOLD MANAGEMENT</h1>
                                </div>
                                <div>
                                    <h3>The alerting threshold management interface allows you to configure Nmon alerting features for:</h3>
                                    <ul>
                                        <li>CPU usage monitoring for continuous configurable peaks in seconds</li>
                                        <li>Physical memory usage monitoring for continuous configurable peaks in seconds</li>
                                        <li>Virtual memory usage monitoring for continuous configurable peaks in seconds</li>
                                    </ul>

                                    <h3>By default, the alerting reports run for all servers with the following parameters:</h3>
                                    <ul>
                                        <li>
                                            <span style="color: indianred">90 %</span> of usage for CPU and Physical memory usage</li>
                                        <li>
                                            <span style="color: indianred">40 %</span> of usage for Virtual memory usage</li>
                                        <li>
                                            <span style="color: indianred">5 minutes (300 seconds)</span> of continuous peak</li>
                                    </ul>

                                    <h3>Alerting reports use KVstore alerting collections to allow a per server configuration:</h3>
                                    <div class="list">
                                        <h4>CPU usage:</h4>
                                        <lu>
                                            <li>alert_cpu_max_percent</li>
                                            <li>alert_cpu_min_seconds</li>
                                        </lu>
                                    </div>
                                    <div class="list">
                                        <h4>Physical memory usage:</h4>
                                        <lu>
                                            <li>alert_physical_memory_max_percent</li>
                                            <li>alert_physical_memory_min_time_seconds</li>
                                        </lu>
                                    </div>
                                    <div class="list">
                                        <h4>Virtual memory usage:</h4>
                                        <lu>
                                            <li>alert_virtual_memory_max_percent</li>
                                            <li>alert_virtual_memory_min_time_seconds</li>
                                        </lu>
                                    </div>
                                    <h3>When a server is matched within the <span style="color: indianred">nmon_alerting_threshold</span> KVstore lookup, the server thresholds settings will override default alerting parameters.</h3>
                                    <h3>Per server alerting entries have the <span style="color: indianred">highest</span> priority against any other settings.</h3>
                                    <h3>Lookup access:</h3>
                                    <pre>| inputlookup nmon_alerting_threshold_filesystem</pre>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Error or failure during the operation -->
                <div class="modal fade" id="modal_generic_error" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header modal-header-danger">
                                <button type="button" class="close" data-dismiss="modal"></button>
                                <h1 style="color: indianred;">
                                    <span class="glyphicon glyphicon-lock"></span>Oops!</h1>
                            </div>
                            <div class="modal-body">
                                <h1>Sorry but it looks like an error occurred while attempting to perform this operation.</h1>
                                <h2>You might not have the permission to do so, or an unexpected failure was encountered.</h2>
                                <b>The server returned the following error message:</b>
                                <div class="modal-error-message">
                                    <p>error return message</p>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default pull-right" data-dismiss="modal">
                                <span class="glyphicon glyphicon-remove"></span> Close</button>
                        </div>
                    </div>
                </div>

                <!-- successful operation -->
                <div class="modal fade" id="modal_generic_success" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header modal-header-danger">
                                <button type="button" class="close" data-dismiss="modal"></button>
                                <h1 style="color: #77dd77;">
                                    <span class="glyphicon glyphicon-lock"></span>Success</h1>
                            </div>
                            <div class="modal-body">
                                <div class="modal-success-message">
                                    <h2>The operation was achieved successfully.</h2>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary pull-right" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span> Ok</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- add entity -->
                <div class="modal custom-modal-80 fade" id="modal_add_entity" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header modal-header-danger">
                                <button type="button" class="close" data-dismiss="modal"></button>
                                <h1 style="color: dodgerblue;">
                                    <span class="glyphicon glyphicon-lock"></span>Add entity:</h1>
                            </div>
                            <div class="modal-body">

                                <div>
                                    <h2>
                                        You can manually add a new entity to the frameID mapping collection, fill the required information and click on submit to add this new entity:
                                    </h2>
                                    <div class="fieldset">
                                        <div class="input input-text" id="input_add_frameid">
                                            <label>set frameID:</label>
                                        </div>
                                        <div class="input input-text" id="input_add_serialnum">
                                            <label>set serialnum:</label>
                                        </div>
                                        <div class="input input-text" id="input_add_host">
                                            <label>set host:</label>
                                        </div>
                                        <div class="input input-text" id="input_add_mount">
                                            <label>set mount:</label>
                                        </div>
                                        <div class="input input-text" id="input_add_alert_fs_max_percent">
                                            <label>set alert_fs_max_percent:</label>
                                        </div>
                                        <div class="input input-text" id="input_add_alert_fs_min_time_seconds">
                                            <label>set alert_fs_min_time_seconds:</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="btn_submit_new_entity" style="margin-right: 10px;" class="btn btn-primary pull-right" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span> Submit</button>
                                <button id="btn_cancel_new_entity" style="margin-right: 10px;" class="btn btn-secondary pull-right" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span> Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- update entity -->
                <div class="modal custom-modal-80 fade" id="modal_update_entity" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header modal-header-danger">
                                <button type="button" class="close" data-dismiss="modal"></button>
                                <h1 style="color: dodgerblue;">
                                    <span class="glyphicon glyphicon-lock"></span>Update or delete entity:</h1>
                            </div>
                            <div class="modal-body">

                                <div>

                                    <h2>
                                        Update the entity information in the following inputs and click on the update button. If you wish to delete this entity from the collection, click on delete.
                                    </h2>

                                    <div class="fieldset">
                                        <div class="input input-text" id="input_update_frameid">
                                            <label>update frameID:</label>
                                        </div>
                                        <div class="input input-text" id="input_update_serialnum">
                                            <label>update serialnum:</label>
                                        </div>
                                        <div class="input input-text" id="input_update_host">
                                            <label>update host:</label>
                                        </div>
                                        <div class="input input-text" id="input_update_mount">
                                            <label>update mount:</label>
                                        </div>
                                        <div class="input input-text" id="input_update_alert_fs_max_percent">
                                            <label>alert_fs_max_percent:</label>
                                        </div>
                                        <div class="input input-text" id="input_update_alert_fs_min_time_seconds">
                                            <label>alert_fs_min_time_seconds:</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button id="btn_submit_delete_entity" style="margin-right: 10px;" class="btn btn-danger pull-right" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span> Delete this entity</button>
                                <button id="btn_submit_update_entity" style="margin-right: 10px;" class="btn btn-primary pull-right" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span> Update this entity</button>
                                <button id="btn_cancel_update_entity" style="margin-right: 10px;" class="btn btn-secondary pull-right" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span> Cancel</button>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- end modals -->

                <div class="parent-top-single">
                    <div id="element_unset_exclusions">
                    </div>
                    <div id="element_nb_hosts">
                    </div>
                    <div id="element_nb_mount">
                    </div>
                </div>

            </html>
        </panel>
    </row>
    <row>
        <panel>
            <html>
                <div>
                    <button id="btn_help" style="margin-left: 20px; margin-right: 10px;" class="btn btn-primary pull-left" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span> Help &amp; information</button>
                    <button id="btn_add_new_template" style="margin-left: 20px; margin-right: 10px;" class="btn btn-primary pull-left" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span>Add a new server threshold</button>
                </div>
            </html>
        </panel>
    </row>

    <row>
        <panel>
            <html>
                <div>

                    <div>
                        <h2>Filters:</h2>
                    </div>

                    <div class="fieldset">

                        <div class="input input-text" id="input_inventory_search_frameid">
                            <label>Search frameID:</label>
                        </div>
                        <div class="input input-text" id="input_inventory_search_serialnum">
                            <label>Search serialnum:</label>
                        </div>
                        <div class="input input-text" id="input_inventory_search_host">
                            <label>Search host:</label>
                        </div>
                        <div class="input input-text" id="input_inventory_search_mount">
                            <label>Search mount point:</label>
                        </div>
                        <div class="input input-text" id="input_inventory_search_filesystem">
                            <label>Search filesystem:</label>
                        </div>

                    </div>

                    <div style="margin-left: 20px;">
                        <div class="input input-linklist" id="inputLink">
                            <label></label>
                        </div>
                    </div>

                    <div id="tableParent1" style="display: none;">
                        <h2>You can search any existing server using the optional fields, fill the filters and press enter.
                        </h2>
                        <h3>
                            <b>alert max value is in percentage and min time is in seconds</b>, mount point is <b>case insensitive and accepts wildcards</b> to match multiple mount points at once
                        </h3>
                        <h3 style="color: dodgerblue;">To create a new rule for a given machine and file-system, click on the corresponding row to open the helper screen.</h3>
                        <div id="element_table_show_lookup_inventory">
                        </div>
                    </div>

                    <div id="tableParent2" style="display: none;">
                        <h2>Threshold table - currently configured alerting thresholds per server</h2>
                        <h3 style="color: dodgerblue;">To update or delete an existing rule, click on the corresponding row to open the helper screen.</h3>
                        <div id="element_table_show_lookup_content">
                        </div>
                    </div>
                </div>
            </html>
        </panel>
    </row>

</dashboard>