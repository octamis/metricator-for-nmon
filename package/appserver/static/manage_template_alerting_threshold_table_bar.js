require([
  "jquery",
  "underscore",
  "splunkjs/mvc",
  "views/shared/results_table/renderers/BaseCellRenderer",
  "splunkjs/mvc/simplexml/ready!",
], function ($, _, mvc, BaseCellRenderer) {
  // bar1
  var DataBarCellRenderer = BaseCellRenderer.extend({
    canRender: function (cell) {
      return cell.field === "max_cpu_percent";
    },
    render: function ($td, cell) {
      var pColor = "data-bar-under";
      if (cell.value > 15) {
        pColor = "data-bar-over";
      }
      $td.addClass("data-bar-cell").html(
        _.template(
          '<div class="data-bar-wrapper"><div class="data-bar <%- pColor %>" style="width:<%- percent %>%">&nbsp;<%- ppp %>%</div></div>',
          {
            percent: Math.min(Math.max(parseFloat(cell.value), 0), 100),
            ppp: parseFloat(cell.value).toFixed(2),
            pColor: pColor,
          }
        )
      );
    },
  });
  // bar2
  var DataBarCellRenderer2 = BaseCellRenderer.extend({
    canRender: function (cell) {
      return cell.field === "max_phy_percent";
    },
    render: function ($td, cell) {
      var pColor = "data-bar-under";
      if (cell.value > 15) {
        pColor = "data-bar-over";
      }
      $td.addClass("data-bar-cell").html(
        _.template(
          '<div class="data-bar-wrapper"><div class="data-bar <%- pColor %>" style="width:<%- percent %>%">&nbsp;<%- ppp %>%</div></div>',
          {
            percent: Math.min(Math.max(parseFloat(cell.value), 0), 100),
            ppp: parseFloat(cell.value).toFixed(2),
            pColor: pColor,
          }
        )
      );
    },
  });
  // bar3
  var DataBarCellRenderer3 = BaseCellRenderer.extend({
    canRender: function (cell) {
      return cell.field === "max_vir_percent";
    },
    render: function ($td, cell) {
      var pColor = "data-bar-under";
      if (cell.value > 15) {
        pColor = "data-bar-over";
      }
      $td.addClass("data-bar-cell").html(
        _.template(
          '<div class="data-bar-wrapper"><div class="data-bar <%- pColor %>" style="width:<%- percent %>%">&nbsp;<%- ppp %>%</div></div>',
          {
            percent: Math.min(Math.max(parseFloat(cell.value), 0), 100),
            ppp: parseFloat(cell.value).toFixed(2),
            pColor: pColor,
          }
        )
      );
    },
  });

  mvc.Components.get("element_table_show_lookup_content").getVisualization(
    function (tableView) {
      tableView.table.addCellRenderer(new DataBarCellRenderer());
      tableView.table.addCellRenderer(new DataBarCellRenderer2());
      tableView.table.addCellRenderer(new DataBarCellRenderer3());
      tableView.table.render();
    }
  );
});
