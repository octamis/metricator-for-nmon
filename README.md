
| branch | build status |
| ---    | ---          |
| master | [![master status](https://circleci.com/bb/octamis/metricator-for-nmon/tree/master.svg?style=svg)](https://circleci.com/bb/octamis/metricator-for-nmon/tree/master)

# Performance Monitor for Unix and Linux Systems

Copyright 2017-2018 Octamis limited - Copyright 2017-2018 Guilhem Marchand

All rights reserved.
